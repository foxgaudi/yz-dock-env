使用教程

1. 当前项目创建shop目录. 克隆项目 https://gitlab.com/foxgaudi/yz-dock-shop 到 shop 目录下
2. 运行docker-compose up -d
3. 把当前yz_development.sql 导入到数据库 yz_development , docker 映射到宿主机端口为3316.
4. 后台登录: localhost:8989  账号/密码  admin/123456
5. 可以在docker-compose.yml ,更改登录端口或者mysql和redis端口映射